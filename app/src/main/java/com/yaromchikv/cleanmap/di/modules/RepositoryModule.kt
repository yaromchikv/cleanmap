package com.yaromchikv.cleanmap.di.modules

import com.yaromchikv.cleanmap.data.api.BankApi
import com.yaromchikv.cleanmap.data.repository.BankRepositoryImpl
import com.yaromchikv.cleanmap.domain.repository.BankRepository
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
object RepositoryModule {

    @Provides
    fun provideRepository(apiService: BankApi) = BankRepositoryImpl(apiService)
}

@Module
interface RepositoryBindModule {

    @Binds
    fun bindBankRepository(bankRepositoryImpl: BankRepositoryImpl): BankRepository
}

