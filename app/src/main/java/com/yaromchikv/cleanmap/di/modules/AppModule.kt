package com.yaromchikv.cleanmap.di.modules

import dagger.Module

@Module(
    includes = [
        NetworkModule::class,
        RepositoryModule::class,
        RepositoryBindModule::class,
        UseCaseModule::class,
        ViewModelModule::class
    ]
)
object AppModule
