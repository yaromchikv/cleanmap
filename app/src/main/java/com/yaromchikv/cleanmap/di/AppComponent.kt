package com.yaromchikv.cleanmap.di

import com.yaromchikv.cleanmap.di.modules.AppModule
import com.yaromchikv.cleanmap.presentation.feature.map.MapsActivity
import com.yaromchikv.cleanmap.presentation.feature.map.MapsViewModel
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(activity: MapsActivity)
}