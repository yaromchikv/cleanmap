package com.yaromchikv.cleanmap.di.modules

import com.yaromchikv.cleanmap.domain.repository.BankRepository
import com.yaromchikv.cleanmap.domain.usecase.CalculateDistanceUseCase
import com.yaromchikv.cleanmap.domain.usecase.GetAtmsUseCase
import com.yaromchikv.cleanmap.domain.usecase.GetFilialsUseCase
import com.yaromchikv.cleanmap.domain.usecase.GetInfoboxesUseCase
import dagger.Module
import dagger.Provides

@Module
object UseCaseModule {

    @Provides
    fun provideCalculateDistanceUseCase() = CalculateDistanceUseCase()

    @Provides
    fun provideGetAtmsUseCase(repository: BankRepository) = GetAtmsUseCase(repository)

    @Provides
    fun provideGetFilialsUseCase(repository: BankRepository) = GetFilialsUseCase(repository)

    @Provides
    fun provideGetInfoboxesUseCase(repository: BankRepository) = GetInfoboxesUseCase(repository)
}