package com.yaromchikv.cleanmap.util

import com.google.android.gms.maps.model.LatLng

object Utils {
    
    const val MAIN_CITY = "Гомель"
    val MAIN_POINT = LatLng(52.425163, 31.015039)
}