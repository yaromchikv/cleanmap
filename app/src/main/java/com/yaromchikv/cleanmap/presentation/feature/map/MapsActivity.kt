package com.yaromchikv.cleanmap.presentation.feature.map

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.yaromchikv.cleanmap.R
import com.yaromchikv.cleanmap.appComponent
import com.yaromchikv.cleanmap.data.dto.AtmDto
import com.yaromchikv.cleanmap.data.dto.InfoboxDto
import com.yaromchikv.cleanmap.databinding.ActivityMapsBinding
import com.yaromchikv.cleanmap.domain.dto.BankObject
import com.yaromchikv.cleanmap.presentation.common.MarkerIconHelper.getMarkerIcon
import com.yaromchikv.cleanmap.util.Utils.MAIN_POINT
import java.net.UnknownHostException
import javax.inject.Inject
import kotlinx.coroutines.flow.collectLatest

class MapsActivity : AppCompatActivity(R.layout.activity_maps) {

    private val binding by viewBinding(ActivityMapsBinding::bind)
    private var googleMap: GoogleMap? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by viewModels<MapsViewModel> { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)

        setContentView(binding.root)
        setupMap()
    }

    private fun setupMap() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync {
            googleMap = it.apply {
                animateCamera(CameraUpdateFactory.newLatLngZoom(MAIN_POINT, 13f))
            }
            setupCollector()
        }
    }

    private fun setupCollector() {
        lifecycleScope.launchWhenStarted {
            viewModel.nearestObjectsState.collectLatest {
                when (it) {
                    is MapsViewModel.State.Ready -> {
                        binding.progressBar.isVisible = false
                        addMarkers(it.data)
                    }
                    is MapsViewModel.State.Error -> {
                        binding.progressBar.isVisible = false
                        showError(it.error)
                    }
                    is MapsViewModel.State.Loading -> {
                        binding.progressBar.isVisible = true
                    }
                    else -> Unit
                }
            }
        }
    }

    private fun addMarkers(listOfObjects: List<BankObject>) {
        listOfObjects.forEach { bankObject ->
            displayMarker(
                position = LatLng(bankObject.latitude, bankObject.longitude),
                title = when (bankObject) {
                    is AtmDto -> getString(R.string.atm_title, bankObject.location)
                    is InfoboxDto -> getString(R.string.infobox_title, bankObject.location)
                    else -> bankObject.location
                },
                snippet = getString(
                    R.string.address,
                    bankObject.streetType,
                    bankObject.street,
                    bankObject.house
                )
            )
        }
    }

    private fun displayMarker(position: LatLng, title: String, snippet: String) {
        googleMap?.addMarker(
            MarkerOptions()
                .icon(getMarkerIcon(this, R.drawable.ic_marker))
                .position(position)
                .title(title)
                .snippet(snippet)
        )
    }

    private fun showError(e: Throwable?) {
        val message = when (e) {
            is UnknownHostException -> getString(R.string.connection_error)
            else -> e?.localizedMessage
        }
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}