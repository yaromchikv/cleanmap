package com.yaromchikv.cleanmap.presentation.common

import android.content.Context
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory

object MarkerIconHelper {

    fun getMarkerIcon(context: Context, @DrawableRes res: Int): BitmapDescriptor {
        val bitmap = AppCompatResources.getDrawable(context, res)?.toBitmap()
        return if (bitmap != null) {
            BitmapDescriptorFactory.fromBitmap(bitmap)
        } else {
            BitmapDescriptorFactory.defaultMarker()
        }
    }
}