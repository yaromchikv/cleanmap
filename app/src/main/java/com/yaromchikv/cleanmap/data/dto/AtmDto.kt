package com.yaromchikv.cleanmap.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.yaromchikv.cleanmap.R
import com.yaromchikv.cleanmap.domain.dto.BankObject

@JsonClass(generateAdapter = true)
data class AtmDto(
    @Json(name = "gps_x") override val latitude: Double,
    @Json(name = "gps_y") override val longitude: Double,
    @Json(name = "install_place") override val location: String,
    @Json(name = "address_type") override val streetType: String,
    @Json(name = "address") override val street: String,
    @Json(name = "house") override val house: String
) : BankObject()