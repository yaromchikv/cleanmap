package com.yaromchikv.cleanmap.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.yaromchikv.cleanmap.domain.dto.BankObject

@JsonClass(generateAdapter = true)
data class FilialDto(
    @Json(name = "GPS_X") override val latitude: Double,
    @Json(name = "GPS_Y") override val longitude: Double,
    @Json(name = "filial_name") override val location: String,
    @Json(name = "street_type") override val streetType: String,
    @Json(name = "street") override val street: String,
    @Json(name = "home_number") override val house: String
) : BankObject()