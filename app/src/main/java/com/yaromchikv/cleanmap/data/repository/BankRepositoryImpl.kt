package com.yaromchikv.cleanmap.data.repository

import com.yaromchikv.cleanmap.data.api.BankApi
import com.yaromchikv.cleanmap.data.dto.AtmDto
import com.yaromchikv.cleanmap.data.dto.FilialDto
import com.yaromchikv.cleanmap.data.dto.InfoboxDto
import com.yaromchikv.cleanmap.domain.repository.BankRepository
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class BankRepositoryImpl @Inject constructor(
    private val apiService: BankApi
) : BankRepository {

    override fun getFilialsList(city: String): Observable<List<FilialDto>> {
        return apiService.getListOfFilials(city)
    }

    override fun getAtmsList(city: String): Observable<List<AtmDto>> {
        return apiService.getListOfATMs(city)
    }

    override fun getInfoboxesList(city: String): Observable<List<InfoboxDto>> {
        return apiService.getListOfInfoboxes(city)
    }
}