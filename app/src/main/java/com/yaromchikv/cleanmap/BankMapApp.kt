package com.yaromchikv.cleanmap

import android.app.Application
import android.content.Context
import com.yaromchikv.cleanmap.di.AppComponent
import com.yaromchikv.cleanmap.di.DaggerAppComponent
import timber.log.Timber

class BankMapApp : Application() {

    lateinit var appComponent: AppComponent
        private set

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.create()
        Timber.plant(Timber.DebugTree())
    }
}

val Context.appComponent: AppComponent
    get() = when (this) {
        is BankMapApp -> appComponent
        else -> this.applicationContext.appComponent
    }
