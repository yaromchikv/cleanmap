package com.yaromchikv.cleanmap.domain.usecase

import com.yaromchikv.cleanmap.data.dto.AtmDto
import com.yaromchikv.cleanmap.domain.repository.BankRepository
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class GetAtmsUseCase(
    private val repository: BankRepository
) {
    operator fun invoke(city: String): Observable<List<AtmDto>> {
        return repository.getAtmsList(city)
    }
}
