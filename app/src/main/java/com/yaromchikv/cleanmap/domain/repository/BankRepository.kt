package com.yaromchikv.cleanmap.domain.repository

import com.yaromchikv.cleanmap.data.dto.AtmDto
import com.yaromchikv.cleanmap.data.dto.FilialDto
import com.yaromchikv.cleanmap.data.dto.InfoboxDto
import io.reactivex.rxjava3.core.Observable

interface BankRepository {
    fun getFilialsList(city: String): Observable<List<FilialDto>>
    fun getAtmsList(city: String): Observable<List<AtmDto>>
    fun getInfoboxesList(city: String): Observable<List<InfoboxDto>>
}