package com.yaromchikv.cleanmap.domain.usecase

import com.yaromchikv.cleanmap.data.dto.InfoboxDto
import com.yaromchikv.cleanmap.domain.repository.BankRepository
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class GetInfoboxesUseCase(
    private val repository: BankRepository
) {
    operator fun invoke(city: String): Observable<List<InfoboxDto>> {
        return repository.getInfoboxesList(city)
    }
}