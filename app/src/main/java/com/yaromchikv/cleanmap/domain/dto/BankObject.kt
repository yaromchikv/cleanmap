package com.yaromchikv.cleanmap.domain.dto

abstract class BankObject {
    abstract val latitude: Double
    abstract val longitude: Double
    abstract val location: String
    abstract val streetType: String
    abstract val street: String
    abstract val house: String
}