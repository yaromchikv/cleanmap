package com.yaromchikv.cleanmap.domain.usecase

import com.yaromchikv.cleanmap.data.dto.FilialDto
import com.yaromchikv.cleanmap.domain.repository.BankRepository
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class GetFilialsUseCase(
    private val repository: BankRepository
) {
    operator fun invoke(city: String): Observable<List<FilialDto>> {
        return repository.getFilialsList(city)
    }
}